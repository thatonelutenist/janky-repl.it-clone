use bollard::container::{
    Config, CreateContainerOptions, InspectContainerOptions, ListContainersOptions,
    StartContainerOptions,
};
use bollard::Docker;
use serde_json;
use tokio::task;
use tungstenite::{accept, connect, Message};
use url::Url;
use uuid::Uuid;

use std::collections::HashMap;
use std::net::TcpListener;

use data::*;

#[tokio::main]
async fn main() {
    // Open Up our port
    let server = TcpListener::bind("0.0.0.0:5001").expect("Unable to bind port");
    let docker = Docker::connect_with_unix_defaults().expect("Unable to connect to docker daemon");
    // To avoid opening and closing docker connections, for now we take the hacky route, and just
    // open one, but leak it, so that it will live as long as our program does
    let docker_ref: &'static Docker = Box::leak(Box::new(docker));

    for stream in server.incoming() {
        if let Ok(stream) = stream {
            task::spawn(async move {
                let mut socket = accept(stream).expect("Failed to accept stream");
                loop {
                    match socket.read_message().expect("Unable to read message") {
                        Message::Text(x) => {
                            let command: OrchCommand = serde_json::from_str(&x).unwrap();
                            let response = handle_command(docker_ref, command).await;
                            let response = serde_json::to_string(&response).unwrap();
                            socket.write_message(Message::Text(response)).unwrap();
                        }
                        _ => break,
                    }
                }
            });
        }
    }
    println!("Hello, world!");
}

/// Dispatches a command to the handling function
async fn handle_command(docker: &'_ Docker, command: OrchCommand) -> OrchResponse {
    match command {
        OrchCommand::CreateContainer(language) => create_container(docker, language).await,
        OrchCommand::CheckContainer(uuid) => check_container(docker, uuid).await,
        OrchCommand::RunCode(command) => run_code(docker, command).await,
    }
}

/// Starts the container with the given UUID
/// Will panic if it fails, internal use only
async fn start_container(docker: &'_ Docker, uuid: Uuid) {
    let uuid_string: String = uuid.to_hyphenated().to_string();
    docker
        .start_container(&uuid_string, None::<StartContainerOptions<String>>)
        .await
        .expect("Unable to start container");
}

/// Creates and starts a container
async fn create_container(docker: &'_ Docker, language: Language) -> OrchResponse {
    let uuid = Uuid::new_v4();
    let uuid_string: String = uuid.to_hyphenated().to_string();
    match language {
        Language::Sbcl => {
            let config = Config {
                image: Some("sbcl"),
                ..Default::default()
            };
            let options = Some(CreateContainerOptions { name: uuid_string });
            docker
                .create_container(options, config)
                .await
                .expect("Unable to create container");
            start_container(docker, uuid).await;
            // Wait 100ms for the container to wake up
            // Janky hack, but tokio_timer 0.3.0 isn't stable yet
            task::block_in_place(|| std::thread::sleep(std::time::Duration::from_millis(100)));
            OrchResponse::NewContainer { uuid }
        }
    }
}

/// returns true if a container exists and is running
async fn container_exists(docker: &'_ Docker, uuid: Uuid) -> bool {
    let uuid_string: String = uuid.to_hyphenated().to_string();
    let mut filters = HashMap::new();
    filters.insert("name", vec![uuid_string.as_str()]);
    // Using filters for this instead off all: false in the options to allow future support for
    // pausing containers
    filters.insert("status", vec!["running"]);
    let options = ListContainersOptions {
        all: true,
        filters,
        ..Default::default()
    };
    let results = docker
        .list_containers(Some(options))
        .await
        .expect("Unable to list containers");
    !results.is_empty()
}

/// Checks to see if a container exists and is running
/// Will return ContainerExists(uuid) if it does, NoSuchContainer(uuid) if it does not
async fn check_container(docker: &'_ Docker, uuid: Uuid) -> OrchResponse {
    if container_exists(docker, uuid).await {
        OrchResponse::ContainerExists { uuid }
    } else {
        OrchResponse::NoSuchContainer { uuid }
    }
}

/// Sends a command to run code to the given container, and returns its response
async fn run_code(docker: &'_ Docker, command: ContainerCommand) -> OrchResponse {
    let uuid = command.session;
    let uuid_string = uuid.to_hyphenated().to_string();
    // First, check to see if the container exists, otherwise we can error early
    if container_exists(docker, uuid).await {
        // Find the container and get the ip
        let container = docker
            .inspect_container(&uuid_string, None::<InspectContainerOptions>)
            .await
            .expect("Unable to inspect container.");
        let ip = container.network_settings.ip_address;
        // Use a hard coded port for now
        let url = Url::parse(&format!("ws://{}:{}", ip, "5000")).unwrap();
        println!("Connecting to {:?}", url);
        let (mut socket, _) = connect(url).expect("Unable to open connection");
        // Stringify the command and send it down the socket
        let command = serde_json::to_string(&command).unwrap();
        socket
            .write_message(Message::Text(command))
            .expect("Unable to send command to container");
        // Read the response
        let response = socket
            .read_message()
            .expect("Unable to read message from container");
        if let Message::Text(response) = response {
            // unstringify the response
            let container_response = serde_json::from_str(&response);
            if let Ok(response) = container_response {
                OrchResponse::CodeResponse(response)
            } else {
                panic!(
                    "Invalid response received from container {:?}:\n{:?}",
                    uuid, response
                )
            }
        } else {
            panic!("Invalid response type received from container {:?}", uuid)
        }
    } else {
        OrchResponse::NoSuchContainer { uuid }
    }
}
