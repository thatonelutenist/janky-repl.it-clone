/// This is actually a really dirty hack. A better solution, for LISPs in
/// particular, would be to write this service in the language itself, and `EVAL`
/// the user input.
///
/// Has currently has the limitation that the output is not allowed to span multiple
/// lines.
use std::io::{BufRead, BufReader, Write};
use std::{
    net::TcpListener,
    process::{Command, Stdio},
};

use tungstenite::{accept, Message};

use data::*;

use serde_json;

fn main() {
    let repl = Command::new("sbcl")
        .args(&["--noinform"])
        .stdout(Stdio::piped())
        .stdin(Stdio::piped())
        .spawn()
        .unwrap();
    let mut input = repl.stdin.unwrap();
    let mut output = BufReader::new(repl.stdout.unwrap());

    let server = TcpListener::bind("0.0.0.0:5000").expect("Unable to bind port");

    for stream in server.incoming() {
        if let Ok(stream) = stream {
            let mut socket = accept(stream).expect("Failed to accept stream");
            loop {
                match socket.read_message().expect("Unable to read message") {
                    Message::Text(x) => {
                        let command: ContainerCommand = serde_json::from_str(&x).unwrap();
                        println!("{:?}", command);
                        input.write_all(command.contents.as_bytes()).unwrap();
                        input.write_all("\n".as_bytes()).unwrap();
                        println!("Wrote to process");
                        let mut buffer = String::new();
                        output.read_line(&mut buffer).unwrap();
                        println!("Read from process");

                        let output = ContainerResponse {
                            session: command.session,
                            stdout: buffer,
                            stderr: "".to_string(),
                        };
                        let output = serde_json::to_string(&output).unwrap();

                        socket.write_message(Message::Text(output)).unwrap();
                        // This arm currently only opens one message and breaks
                        // However, I anticipate having multiple commands in one session
                        socket.write_message(Message::Close(None)).unwrap();
                        break;
                    }
                    _ => break,
                }
            }
        }
    }
}
