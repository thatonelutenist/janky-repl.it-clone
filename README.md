How to Use
==========

1.	`cd sbcl-docker; docker build -t sbcl .; cd..`
2.	In seperate terminals:
	-	`cd orch; cargo run`
	-	`cd web-server; cargo run`
	-	Leave these two processes running
3.	Go to [this repl](https://repl.it/@NathanMcCarty/inteview-project)
4.	Modify the hard coded web socket IP in script.js to your ip
5.	Enable insure websockets on https pages in your browser if need be [guide for firefox](https://stackoverflow.com/questions/11768221/firefox-websocket-security-issue)
6.	Don't forget to Turn those back off when you are done

Limitations
===========

1.	Repl will not behave properly if the output from the users statement is more than one line. (The fault of the server for sbcl)
2.	Repl does not behave properly on empty inputs. (The fault of the server for sbcl)
3.	The server I wrote for SBCL is generally quite janky, and deserves to be written in lisp itself and take advantage of `eval`
4.	While the web-server is coded in an async style, the orch isn't, so spawning more REPLs than you have CPU cores can bog down the executor and result in failure
5.	Web dev isn't my strong suite, so the interface is kinda garbage
6.	Only supports common lisp under SBCL at the moment
7.	Error handling is kinda janky, I need to change the methods to return results to eliminate all the `expect`s and `unwrap`s
