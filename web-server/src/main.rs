use futures::sink::SinkExt;
use futures::stream::StreamExt;
use serde_json;
use tokio::net::{TcpListener, TcpStream};
use tokio::task;
use tokio_tungstenite::{accept_async, connect_async};
use tungstenite::Message;
use url::Url;

use data::*;

#[tokio::main]
async fn main() {
    let addr = "0.0.0.0:5000";
    let mut listener = TcpListener::bind(&addr)
        .await
        .expect("Unable to bind to port");

    while let Ok((stream, _)) = listener.accept().await {
        task::spawn(handle_connection(stream));
    }
}

async fn handle_connection(stream: TcpStream) {
    // Attach to the client stream
    let mut client_ws = accept_async(stream)
        .await
        .expect("Unable to accept client stream");
    // Listen for the first message, we will get the language from this
    let first_message = client_ws
        .next()
        .await
        .unwrap()
        .unwrap()
        .into_text()
        .unwrap();
    // Destringify the message
    let command: WebServerCommand =
        serde_json::from_str(&first_message).expect("Unable to parse first command");
    // Now that we know the client has sent us a valid first command, we must open a connection to
    // the orchestrator
    // We assume the orchestrator is running on the local machine for now
    let orch_url = "ws://localhost:5001";
    let (mut orch_ws, _) = connect_async(Url::parse(orch_url).unwrap())
        .await
        .expect("Unable to connect to orchestrator");
    // Ask the orchestrator to make us repl
    orch_ws
        .send(Message::Text(
            serde_json::to_string(&OrchCommand::CreateContainer(command.language)).unwrap(),
        ))
        .await
        .expect("Unable to send command to create container to orchestrator");
    // Await the response, and parse the uuid out of it
    let response = orch_ws
        .next()
        .await
        .expect("Unable to receive response from orchestrator")
        .expect("Unable to receive response from orchestrator")
        .into_text()
        .unwrap();
    let response: OrchResponse =
        serde_json::from_str(&response).expect("Recieved invalid response from orchestrator");
    let uuid = if let OrchResponse::NewContainer { uuid } = response {
        uuid
    } else {
        panic!()
    };
    // now that we have the container we can execute the users command
    let mut last_command = Some(command);
    while let Some(command) = last_command {
        // Run the users code
        let container_command = ContainerCommand {
            session: uuid,
            contents: command.contents.clone(),
        };
        let orch_command = OrchCommand::RunCode(container_command);
        orch_ws
            .send(Message::Text(serde_json::to_string(&orch_command).unwrap()))
            .await
            .unwrap();
        let orch_response = orch_ws.next().await.unwrap().unwrap().into_text().unwrap();
        let orch_response: OrchResponse = serde_json::from_str(&orch_response).unwrap();
        let orch_response: ContainerResponse =
            if let OrchResponse::CodeResponse(resp) = orch_response {
                resp
            } else {
                panic!()
            };
        let resp = WebServerResponse::from_container_response(orch_response);
        client_ws
            .send(Message::Text(serde_json::to_string(&resp).unwrap()))
            .await
            .unwrap();
        let mut text = false;
        let mut client_response = client_ws.next().await.unwrap().unwrap();
        while !text {
            match client_response {
                Message::Text(_) => text = true,
                Message::Ping(x) => {
                    client_ws.send(Message::Pong(x)).await.unwrap();
                    client_response = client_ws.next().await.unwrap().unwrap();
                }
                _ => panic!("Invalid client Response"),
            }
        }

        // Get the next command
        last_command = Some(serde_json::from_str(&client_response.into_text().unwrap()).unwrap());
    }
}
