use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct ContainerCommand {
    pub session: Uuid,
    pub contents: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ContainerResponse {
    pub session: Uuid,
    pub stdout: String,
    pub stderr: String,
}

#[derive(Debug, Deserialize, Serialize, Clone, Copy)]
pub enum Language {
    Sbcl,
}

#[derive(Debug, Deserialize, Serialize)]
pub enum OrchCommand {
    /// Command to create a new container, and provide back the UUID
    CreateContainer(Language),
    /// Command to Check to see if a container exists
    CheckContainer(Uuid),
    /// Command to run the given command on the correct container
    RunCode(ContainerCommand),
}

#[derive(Debug, Deserialize, Serialize)]
pub enum OrchResponse {
    NewContainer { uuid: Uuid },
    CodeResponse(ContainerResponse),
    ContainerExists { uuid: Uuid },
    NoSuchContainer { uuid: Uuid },
}

#[derive(Debug, Deserialize, Serialize)]
pub struct WebServerCommand {
    pub language: Language,
    pub contents: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct WebServerResponse {
    pub stdout: String,
    pub stderr: String,
}

impl WebServerResponse {
    pub fn from_container_response(resp: ContainerResponse) -> Self {
        WebServerResponse {
            stdout: resp.stdout,
            stderr: resp.stderr,
        }
    }
}
